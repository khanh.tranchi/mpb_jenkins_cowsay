pipeline {
    agent any

    environment{
        BRANCH_NAME = "main"
    }

    options {
        //Disallow concurrent executions of the Pipeline.
        // Preventing simultaneous accesses to shared resources
        disableConcurrentBuilds()
    }

    stages {
        stage('Build'){
            when(){
                // anyOf { BRANCH_NAME 'main'; branch 'feature/*' }
                anyOf {
                    environment name: 'BRANCH_NAME', value: 'main';
                    environment name: 'BRANCH_NAME', value: 'feature/*';
                }
            }
            steps{
                //Clone
                git branch: env.BRANCH_NAME, credentialsId: 'Gitlab_PK', url: 'git@gitlab.com:khanh.tranchi/mpb_jenkins_cowsay.git'

                script{
                    // Build
                    def cowsay_image = docker.build "khanhtranc/mbp-jenkins-cowsay"

                    // Run and Test
                    cowsay_image.withRun('--rm --name mbp-jenkins-cowsay --network jenkins_docker') {c ->
                        sh  '''
                                sleep 5
                                curl "http://mbp-jenkins-cowsay:8080"
                            '''
                    }
                }
            }
        }
        stage('Deploy') {
            when(){
                // branch "main" // default of gitlab is main
                environment name: 'BRANCH_NAME', value: 'main';
            }
            steps {
                script{
                    // Push to registry
                    docker.withRegistry('', 'hub.docker.com') {
                        docker.image('khanhtranc/mbp-jenkins-cowsay').push('latest')
                    }

                    // Deploy local
                    sh  '''
                            container_id=$(docker ps -f "name=mbp-jenkins-cowsay-pro" -q)
                            if [ ! -z "$container_id" ]
                            then
                                docker stop $container_id
                            fi
                            docker pull khanhtranc/mbp-jenkins-cowsay
                            docker run -d --rm -p 8081:8080 --name mbp-jenkins-cowsay-pro khanhtranc/mbp-jenkins-cowsay
                        '''
                }
            }
        }
    }
    post{
        always {
            sh 'docker logout'
            sh 'git clean -f'
        }
        failure{
            script{
                if(BRANCH_NAME.startsWith("feature")){
                    emailext to: "dev@gmail.com",
                        subject: "jenkins build:${currentBuild.currentResult}: ${env.JOB_NAME}",
                        body: "${currentBuild.currentResult}: Job ${env.JOB_NAME}\nMore Info can be found here: ${env.BUILD_URL}"
                }
            }
        }
    }
}